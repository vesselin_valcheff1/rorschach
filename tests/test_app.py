import pytest
import yaml
from fastapi.testclient import TestClient
from rorschach.app import app

client = TestClient(app)
config = yaml.safe_load(open("rorschach/config/config.yml"))


@pytest.mark.parametrize(
    "admin_key, user_key_duration, user_key_uses, expected, status_code",
    [
        (
            "not_true_admin_key",
            10,
            10,
            '{"detail":"Invalid admin_key."}',
            401,
        ),
    ],
)
def test_generate_user_key(
    admin_key, user_key_duration, user_key_uses, expected, status_code
):
    response = client.post(
        f"/generate_user_key/?admin_key={admin_key}&user_key_duration={user_key_duration}\
        &user_key_uses={user_key_uses}",
    )
    assert response.status_code == status_code
    assert response.text == expected
