import pytest
import yaml
from rorschach.nst import NST

style_transferer = NST()
config = yaml.safe_load(open("rorschach/config/config.yml"))


@pytest.mark.parametrize(
    "image_file, style, method, expected_shape",
    [
        (
            "tests/test_images/starry_night.jpg",
            "starry_night",
            "quick",
            (512, 384),
        ),
        (
            "tests/test_images/dynamism_cyclist.png",
            "starry_night",
            "quick",
            (512, 380),
        ),
    ],
)
def test_transfer_style(image_file, style, method, expected_shape):
    with open(image_file, "rb") as raw_image:
        stylized_image = style_transferer.transfer_style(raw_image, style, method)
        assert stylized_image.size == expected_shape
