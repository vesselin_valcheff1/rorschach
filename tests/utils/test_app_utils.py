import pytest
from rorschach.utils.app_utils import allowed_style, allowed_file


@pytest.mark.parametrize(
    "input_str, expected",
    [
        ("filename.jpg", True),
        ("filenamejpg", False),
        ("filename.png", True),
    ],
)
def test_allowed_file(input_str, expected):
    actual = allowed_file(input_str)
    assert actual == expected


@pytest.mark.parametrize(
    "input_str, expected",
    [
        ("starry_night", True),
        ("wheatfield", True),
        ("starrynight", False),
    ],
)
def test_allowed_style(input_str, expected):
    actual = allowed_style(input_str)
    assert actual == expected
