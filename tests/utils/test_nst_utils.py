import pytest
from rorschach.utils.nst_utils import decode_img


@pytest.mark.parametrize(
    "image, expected",
    [
        ("tests/test_images/starry_night.jpg", (1, 383, 512, 3)),
        ("tests/test_images/wheatfield.jpg", (1, 339, 512, 3)),
    ],
)
def test_decode_img(image, expected):
    with open(image, "rb") as raw_image:
        decoded_img = decode_img(raw_image)
        assert decoded_img.shape == expected
