FROM python:3.8-slim

COPY requirements.txt /opt/app/
COPY rorschach /opt/app/rorschach

WORKDIR /opt/app/

RUN pip install -r requirements.txt --no-cache-dir

ENTRYPOINT ["gunicorn", "rorschach.app:app", "--worker-class", "uvicorn.workers.UvicornH11Worker", "--bind", "0.0.0.0:8000"]