# This is the repository for the Neural Style Transfer demo project by Veselin Valchev
Neural style transfer is a beautiful tool in showcasing the capacity and 
capabilities of neural networks, specifically convolutional neural networks in that
they can quantify abstractions that seem unquantifiable to us humans.

For example, artistic style.

Neural style stransfer, to the best of my knowledge originating from the
"A Neural Algorithm of Artistic Style" paper by Leon A. Gatys, Alexander S. Ecker and Matthias Bethge
aims to:</br>

First, quantify the style of one image, say Van Gogh's starry night:</br>
![Imgur](https://imgur.com/NusvPR0.jpg )
</br>

Then, quantify the content of another image (crazy as it is), say my parent's house:</br>
![Imgur](https://imgur.com/MDsunI9.jpg)
</br>

And finally, implant the style of the first image onto the second image, by way of bringing the vector representations of the images (the quantified state of their style and contens) closer together, producing fascinating visuals:</br>
![Imgur](https://imgur.com/gVvaCoo.jpg)
</br>


## API
The API that runs on the code within this repository is created in good faith and does not aim to infringe on any rights.</br>

The API is STRICTLY for demonstration purposes and does not aim to generate any commercial value.</br>

The API can be accessed freely here: https://neural-style-transfer-vmekf4jrbq-nw.a.run.app/docs</br>

The link above leads to the Swagger documentation page, which can serve as a rudimentary UI for the API.</br>

It runs on a CloudRun service in GoogleCloudPlatform.</br>

Please note that the URL might take a while to load initially.</br>

In order to be able to use the API, please contact me at vesselin.valcheff@gmail.com to generate a key for you.</br>

Opening the link above, you will see something like:</br>
![Imgur](https://imgur.com/UPKBkFr.jpg)

Click on the green line with '/neural_style_transfer/' and then on 'Try It Out' in the top right corner.</br>
You will then see a window like:</br>
![Imgur](https://imgur.com/pah1lEe.jpg)

First, upload the file you want to transfer style onto.</br>
Then, select the target style - limited choice available at the moment, displayed next to the form.</br>
Then, select the method for transfer - limited choice at the moment, displayed next to the form.</br>
Finally, provide the user_key provided by me</br>
Clicking on the blue 'execute' button will generate the image (takes 10-20 seconds), which you will then be able to download with the 'Download file' link:</br>
![Imgur](https://imgur.com/qAlMAdh.jpg)

Each user_key is generated with a set expiry_date and number of uses.

Planned improvements:
1. Add 'slow' method

Reach out to me at vesselin.valcheff@gmail.com