import yaml
from fastapi import FastAPI, File, Form, UploadFile, HTTPException
from fastapi.responses import StreamingResponse
from io import BytesIO
from rorschach.utils import app_utils
from rorschach.nst import NST

config = yaml.safe_load(open("rorschach/config/config.yml"))
style_transferer = NST()

tags_metadata = [
    {
        "name": "Neural Style Transfer",
        "description": "Transfer the style of one image onto the content of another image.",
    }
]

app = FastAPI(
    title="Neural Style Transfer Demo API by Veselin Valchev",
    description="Transfer the style of one image onto the content of another image. To do that,"
    "we employ neural networks (hence NEURAL style transfer), in order to quantify"
    "something even as abstract as style. The transfer engine is based on Gatys' et. al's"
    " paper https://arxiv.org/abs/1508.06576, and incorporates code and models from the"
    " tensorflow tutorial at https://www.tensorflow.org/tutorials/generative/style_transfer."
    "</br></br></br>"
    "DISCLAIMER: This tool is for non-commercial use only, and has been created in good faith,"
    " and with no intention to infringe on any rights.",
    version="0.1.1",
    openapi_tags=tags_metadata,
    root_path="/",
)


@app.post("/neural_style_transfer/", tags=["Neural Style Transfer"])
async def neural_style_transfer(
    file: UploadFile = File(
        ...,
        description=f"Upload the image \
to be transformed to another style. File must be one of: {config['allowed_extensions']}.",
    ),
    style: str = Form(
        "starry_night",
        description=f"Enter the source style \
to transfer onto your content image. Must be one of: {config['allowed_styles']}.",
    ),
    method: str = Form(
        "quick",
        description=f"Enter the method by which to perform style transfer. \
        Must be one of: {config['allowed_methods']}. 'quick' uses a pretrained algorithm "
        f"found at https://tfhub.dev/google/magenta/arbitrary-image-stylization-v1-256/2.",
    ),
    user_key: str = Form(
        "None",
        description="Enter the user_key provided by an admin.",
    ),
):
    image_file = file.file
    if not app_utils.allowed_file(file.filename):
        raise HTTPException(
            status_code=422,
            detail="Image file provided not in allowed formats.",
        )
    if not app_utils.allowed_style(style):
        raise HTTPException(
            status_code=422,
            detail="Style requested not in allowed styles.",
        )
    if not app_utils.allowed_method(method):
        raise HTTPException(
            status_code=422,
            detail="Method requested not in allowed methods.",
        )
    if not app_utils.sanitize_input(user_key):
        raise HTTPException(
            status_code=422,
            detail="User key contains invalid characters. Key must be URL-safe.",
        )
    key_checker = app_utils.KeyChecker()
    if key_checker.user_key_exists(user_key):
        if key_checker.key_not_expired(user_key):
            if not key_checker.uses_remaining(user_key):
                raise HTTPException(
                    status_code=401,
                    detail="User_key has no more uses remaining.",
                )
        else:
            raise HTTPException(
                status_code=401,
                detail="User_key has expired.",
            )
    else:
        raise HTTPException(
            status_code=401,
            detail="Invalid user_key.",
        )
    stylized_image = style_transferer.transfer_style(image_file, style, method)
    # amend the uses remaining accordingly
    app_utils.decrement_remaining_uses(user_key)
    in_memory_fp = BytesIO()
    stylized_image.save(in_memory_fp, "PNG")
    in_memory_fp.seek(0)
    headers = {"Content-Disposition": 'attachment; filename="stylized_image.png"'}
    return StreamingResponse(in_memory_fp, media_type="image/png", headers=headers)


@app.post("/generate_user_key/", tags=["Endpoint to generate user keys by admin."])
async def generate_user_key(
    admin_key: str = Form(
        "None",
        description="Admin key.",
    ),
    user_key_duration: int = Form(
        10,
        description="The duration in days for which the key will be valid, starting today.",
    ),
    user_key_uses: int = Form(
        10,
        description="The number of uses that the key will be valid for.",
    ),
):
    if not app_utils.sanitize_input(admin_key):
        raise HTTPException(
            status_code=422,
            detail="Admin_key contains invalid characters. Must be URL-safe.",
        )
    if not app_utils.sanitize_input(user_key_duration):
        raise HTTPException(
            status_code=422,
            detail="User_key_duration contains invalid characters. Must be URL-safe.",
        )
    if not app_utils.sanitize_input(user_key_uses):
        raise HTTPException(
            status_code=422,
            detail="User_key_uses contains invalid characters. Must be URL-safe.",
        )
    key_checker = app_utils.KeyChecker()
    if not key_checker.admin_key_exists(admin_key):
        raise HTTPException(
            status_code=401,
            detail="Invalid admin_key.",
        )
    new_user_key = app_utils.create_new_key(user_key_duration, user_key_uses)
    return new_user_key
