import yaml
import re
from google.cloud import bigquery, bigquery_storage  # type: ignore
from datetime import datetime, timedelta
import numpy as np
import secrets
import pandas as pd

config = yaml.safe_load(open("rorschach/config/config.yml"))
allowed_extensions = config["allowed_extensions"]
allowed_styles = config["allowed_styles"]
allowed_methods = config["allowed_methods"]

project = config["bq_project"]
bqclient = bigquery.Client(project=project)
bqstorageclient = bigquery_storage.BigQueryReadClient()


def sanitize_input(string_to_sanitize):
    # enforce url-unreserved chars
    if re.match("^[A-Za-z0-9_-]*$", str(string_to_sanitize)):
        return True
    else:
        return False


def allowed_file(filename):
    return "." in filename and filename.split(".")[-1].lower() in allowed_extensions


def allowed_style(style):
    return style in allowed_styles


def allowed_method(method):
    return method in allowed_methods


def decrement_remaining_uses(user_key):
    query = f"""
UPDATE api_keys.api_keys
SET uses_remaining = uses_remaining - 1
WHERE key = '{user_key}'
"""
    job_config = bigquery.QueryJobConfig()
    query_job = bqclient.query(query, job_config=job_config)  # Make an API request.
    query_job.result()  # Wait for the job


def create_new_key(duration, uses):
    token = secrets.token_urlsafe(20)
    # to make sure...
    assert sanitize_input(token)
    df = pd.DataFrame(
        {
            "key": [token],
            "key_type": ["user"],
            "create_date": [datetime.now()],
            "expiry_date": [datetime.now() + timedelta(duration)],
            "uses_total": [uses],
            "uses_remaining": [uses],
        }
    )
    job_config = bigquery.LoadJobConfig(
        # Specify a (partial) schema. All columns are always written to the
        # table. The schema is used to assist in data type definitions.
        schema=[
            bigquery.SchemaField("key", bigquery.enums.SqlTypeNames.STRING),
            bigquery.SchemaField("key_type", bigquery.enums.SqlTypeNames.STRING),
            bigquery.SchemaField("create_date", bigquery.enums.SqlTypeNames.TIMESTAMP),
            bigquery.SchemaField("expiry_date", bigquery.enums.SqlTypeNames.TIMESTAMP),
            bigquery.SchemaField("uses_total", bigquery.enums.SqlTypeNames.INTEGER),
            bigquery.SchemaField("uses_remaining", bigquery.enums.SqlTypeNames.INTEGER),
        ],
        # Optionally, set the write disposition. BigQuery appends loaded rows
        # to an existing table by default, but with WRITE_TRUNCATE write
        # disposition it replaces the table with the loaded data.
        write_disposition="WRITE_APPEND",
    )

    job = bqclient.load_table_from_dataframe(
        df, "rorshach.api_keys.api_keys", job_config=job_config
    )  # Make an API request.
    job.result()  # Wait for the job to complete.
    return token


class KeyChecker:
    def __init__(self):
        query = f"Select * from `{project}.api_keys.api_keys`"
        self.keys_df = (
            bqclient.query(query)
            .result()
            .to_dataframe(bqstorage_client=bqstorageclient)
        )

    def key_not_expired(self, user_key):
        row_to_check = self.keys_df[self.keys_df["key"] == user_key]
        return np.datetime64(datetime.now()) <= row_to_check["expiry_date"].values[0]

    def uses_remaining(self, user_key):
        row_to_check = self.keys_df[self.keys_df["key"] == user_key]
        return row_to_check["uses_remaining"].values[0] > 0

    def user_key_exists(self, user_key):
        return (
            user_key in self.keys_df[self.keys_df["key_type"] == "user"]["key"].tolist()
        )

    def admin_key_exists(self, admin):
        return (
            admin in self.keys_df[self.keys_df["key_type"] == "admin"]["key"].tolist()
        )
