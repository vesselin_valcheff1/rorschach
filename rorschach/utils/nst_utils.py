import tensorflow as tf
import yaml
import numpy as np
import PIL

config = yaml.safe_load(open("rorschach/config/config.yml"))
max_dim = config["max_dim"]


def decode_img(img):
    # convert the compressed string to a 3D uint8 tensor
    img = tf.io.decode_image(img.read())
    img = tf.image.convert_image_dtype(img, tf.float64)
    # get the shapeof the original image, and it's longer dimension
    shape = tf.cast(tf.shape(img)[:-1], tf.float64)
    long_dim = max(shape)
    # get the scale of the max dimension to the longer dimension
    scale = max_dim / long_dim
    # get the new shape, and reshape the image, adding the new axis
    new_shape = tf.cast(shape * scale, tf.int32)
    img = tf.image.resize(img, new_shape)
    img = img[tf.newaxis, :]
    return img


def tensor_to_image(tensor):
    tensor = tensor * 255
    tensor = np.array(tensor, dtype=np.uint8)
    if np.ndim(tensor) > 3:
        assert tensor.shape[0] == 1
        tensor = tensor[0]
    return PIL.Image.fromarray(tensor)
