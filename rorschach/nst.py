import yaml
from rorschach.utils.nst_utils import decode_img, tensor_to_image
import tensorflow_hub as hub
import tensorflow as tf

config = yaml.safe_load(open("rorschach/config/config.yml"))
# google recommends we don't persist model and re-download... let's see
hub_model = hub.load(
    "https://tfhub.dev/google/magenta/arbitrary-image-stylization-v1-256/2"
)


class NST:
    def __init__(self):
        self.max_dim = config["max_dim"]

    def quick_transfer(self, content_tensor, style_tensor):
        stylized_image = hub_model(
            tf.constant(content_tensor), tf.constant(style_tensor)
        )[0]
        stylized_image = tensor_to_image(stylized_image)
        return stylized_image

    def transfer_style(self, image_file, style, method):
        # we need to feed the images forward as tensors
        content_tensor = decode_img(image_file)
        with open(f"rorschach/styles_images/{style}.jpg", "rb") as raw_style:
            style_tensor = decode_img(raw_style)
        # branch out here depending on method
        if method == "quick":
            stylized_image = self.quick_transfer(content_tensor, style_tensor)
        else:
            stylized_image = self.quick_transfer(content_tensor, style_tensor)
        return stylized_image
